package com.lennon.spring.basic;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.lennon.spring.basic.service.Gitar;

public class MainXml {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new FileSystemXmlApplicationContext
	            ("classpath:application-config.xml");
		
		Gitar fenderObject = (Gitar) context.getBean("fender");
	    fenderObject.play();
	    
	    Gitar gibsonObject = (Gitar) context.getBean("gibson");
	    gibsonObject.play();
	}

}
