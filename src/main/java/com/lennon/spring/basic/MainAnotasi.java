package com.lennon.spring.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.lennon.spring.basic.service.Musik;

public class MainAnotasi {
	
	@Autowired
	private static Musik musik;
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new FileSystemXmlApplicationContext
	            ("classpath:application-config.xml");
		
		Musik pyschedelicObject = (Musik) context.getBean("psychedelic");
		pyschedelicObject.play();

		Musik klasikObject = (Musik) context.getBean("klasikAnotasi");
		klasikObject.play();
		
		String[] springArray = context.getBeanDefinitionNames();
		for (int x=0; x<springArray.length; x++){
			System.out.println(springArray[x].toString());
		}

	}
}


