package com.lennon.spring.basic.service.impl;

import com.lennon.spring.basic.service.Gitar;

public class Fender implements Gitar {
	
	private String nama;
	
	@Override
	public void play() {
		System.out.println(getNama());
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

}
