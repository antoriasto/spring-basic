package com.lennon.spring.basic.service.impl;

import org.springframework.stereotype.Service;

import com.lennon.spring.basic.service.Musik;

@Service("klasikAnotasi")
public class Klasik implements Musik {

	@Override
	public void play() {
		System.out.println("Ini musik klasik");
	}

}
